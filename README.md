# cat4kit_ui_frontend

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Customize configuration
Make a configuration file named `config.js` in the `src` directory of the project. The file should contain the following variables:

```js
let config;
config = {
    $axios_url_backend: "{Django backend url}",
    $axios_url_bridge: "{Django bridge url to be connected to STAC-API}",
};

export { config }
```

```sh
See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```
