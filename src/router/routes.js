export default [

  {
    path: '/',
    component: () => import('@/layouts/DashboardLayout.vue'),
    children: [
      {
        path: '/dashboard',
        name: 'Dashboard',
        component: () => import('@/views/Dashboard.vue'),
      },
      {
        path: '/ds2stac/tds2stac',
        name: 'TDS2STAC',
        component: () => import('@/views/ds2stac/TDS2STACView.vue'),
      },
      {
        path: '/ds2stac/intake2stac',
        name: 'INTAKE2STAC',
        component: () => import('@/views/ds2stac/INTAKE2STACView.vue'),
      },
      {
        path: '/ds2stac/sta2stac',
        name: 'STA2STAC',
        component: () => import('@/views/ds2stac/STA2STACView.vue'),
      },
      {
        path: '/testjobs',
        name: 'TestJobs',
        component: () => import('@/views/TestJobs.vue'),
      },
      {
        path: '/apiresolver',
        name: 'APIResolver',
        component: () => import('@/views/APIResolverView.vue'),
      },
      {
        path: '/scheduler',
        name: 'Scheduler',
        component: () => import('@/views/SchedulerView.vue'),
      },
      {
        path: '/Logger',
        name: 'Logger',
        component: () => import('@/views/LoggerView.vue'),
      },
      {
        path: '/Feedback',
        name: 'Feedback',
        component: () => import('@/views/FeedBackView.vue'),
      },
    ],
  },
  {
    path: '/auth',
    name: 'Auth',
    component: () => import('@/layouts/AuthenticationLayout.vue'),
    children: [
      {
        path: '/auth/login/:message?',
        name: 'Login',
        component: () => import('@/views/auth/Login.vue'),
      },
      {
        path: '/auth/register',
        name: 'Register',
        component: () => import('@/views/auth/Register.vue'),
      },
      {
        path: '/auth/forgot-password',
        name: 'ForgotPassword',
        component: () => import('@/views/auth/ForgotPassword.vue'),
      },
      {
        path: '/auth/reset-password',
        name: 'ResetPassword',
        component: () => import('@/views/auth/ResetPassword.vue'),
      },
      {
        path: '/auth/confirm-password',
        name: 'ConfirmPassword',
        component: () => import('@/views/auth/ConfirmPassword.vue'),
      },
      {
        path: '/auth/verify-email',
        name: 'VerifyEmail',
        component: () => import('@/views/auth/VerifyEmail.vue'),
      },
    ],
  },
]
