import { createRouter, createWebHashHistory } from 'vue-router'
import NProgress from 'nprogress'
import routes from '@/router/routes'
import { sidebarState } from '@/composables'
import axios from 'axios';

const router = createRouter({
  history: createWebHashHistory('kui-dashboard-vue'),
  routes,
})

// router.beforeEach(() => {
//   NProgress.start()
// })
// In your router/index.js or where you define Vue Router
// router.beforeEach(async (to, from, next) => {
router.beforeEach(() => {
  NProgress.start()
  // const aaibool_storage = localStorage.getItem('user.helmholtz');
  // const aaiemail_storage = localStorage.getItem('user.email');

  // if (!aaiemail_storage || !aaibool_storage) {
  //     clearAuthorization();
  //     redirectToLogin('Oops! Something went wrong. Login again');
  //     return;
  // }

  // try {
  //     const response = await axios.post(getApiUrl(aaibool_storage), { email: aaiemail_storage });
  //     handleResponse(response);
  // } catch (error) {
  //     console.log('error', error);
  //     clearAuthorization();
  //     redirectToLogin("Connection error. Please try again.");
  // }

  // function getApiUrl(aaibool_storage) {
  //     return aaibool_storage === 'true' ? '/api/helmholtz_aai_refresh_token/' : '/api/refresh_token/';
  // }

  // function handleResponse(response) {
  //     if (response.data.length === 0) {
  //         clearAuthorization();
  //         redirectToLogin();
  //     } else if (response.data.message) {
  //         clearAuthorization();
  //         redirectToLogin(response.data.message);
  //     } else if (response.data.access_token && response.data.refresh_token) {
  //         axios.defaults.headers.common['Authorization'] = `${response.data.token_type} ${response.data.access_token}`;
  //         next();
  //         // if (to.name !== 'TestJobs') {
  //         //     next({ name: 'TestJobs' });
  //         // } else {
  //         //     next();
  //         // }
  //     } else {
  //         clearAuthorization();
  //         redirectToLogin('Oops! Something went wrong. Login again');
  //     }
  // }

  // function clearAuthorization() {
  //     axios.defaults.headers.common['Authorization'] = '';
  // }

  // function redirectToLogin(message = '') {
  //     next({ name: 'Login', params: { message } });
  // }
});
router.afterEach(() => {
  if (window.innerWidth <= 1024) {
    sidebarState.isOpen = false
  }
  NProgress.done()
})

export default router