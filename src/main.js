import '@/assets/css/main.css'
import axios from 'axios'
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import Toast from "vue-toastification";
// Import the CSS or use your own!
import "vue-toastification/dist/index.css";
import VueTailwindDatepicker from 'vue-tailwind-datepicker'
import VueScrollTo from 'vue-scrollto'
import HighchartsVue from 'highcharts-vue'
import { config } from './config';
import App from './App.vue'
import { InstallCodemirro } from "codemirror-editor-vue3" 


import router from './router'
console.log(config)
axios.defaults.baseURL = config.$axios_url_backend

const app = createApp(App)
app.use(VueTailwindDatepicker)
app.use(HighchartsVue)
app.use(createPinia())
app.use(router,axios)
app.use(InstallCodemirro) 
app.use(Toast, {
  position: "top-right",
  timeout: 5000,
  closeOnClick: true,
  pauseOnFocusLoss: true,
  pauseOnHover: true,
  draggable: true,
  draggablePercent: 0.6,
  showCloseButtonOnHover: false,
  hideProgressBar: false,
  closeButton: "button",
  icon: true,
  rtl: false
})
app.use(VueScrollTo)
app.mount('#app')
