import { defineStore } from 'pinia'
import axios from 'axios'

export const useUserStore = defineStore({
    id: 'user',
    state: () => ({
        user: {
            isAuthenticated: false,
            id: null,
            name: null,
            email: null,
            access: null,
            refresh: null,
            avatar: null,
            helmholtz: 'false'
        }
    }),
    actions: {
        aaiInitStore() {
            console.log('aaiInitStore')
            this.user.email = localStorage.getItem('user.email')
            if (sessionStorage.getItem('user.access')) {
                this.setTokenSession2Local()
                console.log('User has access to sessionStorage!')
                this.user.access = sessionStorage.getItem('user.access')
                this.user.refresh = sessionStorage.getItem('user.refresh')
                this.user.isAuthenticated = sessionStorage.getItem('user.isAuthenticated')
                this.user.id = sessionStorage.getItem('user.id')
                this.user.name = sessionStorage.getItem('user.name')
                this.user.email = sessionStorage.getItem('user.email')
                this.user.avatar = localStorage.getItem('user.avatar')
                this.user.helmholtz = sessionStorage.getItem('user.helmholtz')  
            } else if (localStorage.getItem('user.access')) {
                console.log('User has access to localStorage!')
                this.user.access = localStorage.getItem('user.access')
                this.user.refresh = localStorage.getItem('user.refresh')
                this.user.isAuthenticated = localStorage.getItem('user.isAuthenticated')
                this.user.id = localStorage.getItem('user.id')
                this.user.name = localStorage.getItem('user.name')
                this.user.email = localStorage.getItem('user.email')
                this.user.avatar = localStorage.getItem('user.avatar') 
                this.user.helmholtz = localStorage.getItem('user.helmholtz')               
                this.refreshToken()
            }
        },





        initStore() {
            
            if (sessionStorage.getItem('user.access')) {
                this.setTokenSession2Local()
                console.log('User has access to sessionStorage!')
                this.user.access = sessionStorage.getItem('user.access')
                this.user.refresh = sessionStorage.getItem('user.refresh')
                this.user.isAuthenticated = sessionStorage.getItem('user.isAuthenticated')
                this.user.id = sessionStorage.getItem('user.id')
                this.user.name = sessionStorage.getItem('user.name')
                this.user.email = sessionStorage.getItem('user.email')
                this.user.avatar = localStorage.getItem('user.avatar')
                this.user.helmholtz = sessionStorage.getItem('user.helmholtz')  
            } else if (localStorage.getItem('user.access')) {
                console.log('User has access to localStorage!')
                this.user.access = localStorage.getItem('user.access')
                this.user.refresh = localStorage.getItem('user.refresh')
                this.user.isAuthenticated = localStorage.getItem('user.isAuthenticated')
                this.user.id = localStorage.getItem('user.id')
                this.user.name = localStorage.getItem('user.name')
                this.user.email = localStorage.getItem('user.email')
                this.user.avatar = localStorage.getItem('user.avatar') 
                this.user.helmholtz = localStorage.getItem('user.helmholtz')               
                this.refreshToken()
            }                

        },
        setToken(data) {
            console.log('setToken', data)

            this.user.access = data.access
            this.user.refresh = data.refresh
            this.user.isAuthenticated = true
            this.user.helmholtz = false

            localStorage.setItem('user.access', data.access)
            localStorage.setItem('user.refresh', data.refresh)
            localStorage.setItem('user.helmholtz', 'false')

            console.log('user.access: ', localStorage.getItem('user.access'))
        },
        setTokenSession2Local() {
            console.log('setTokenSession2Local1',sessionStorage.getItem('user.access'))
            localStorage.setItem('user.access', JSON.stringify(sessionStorage.getItem('user.access')))
            console.log('setTokenSession2Local2',localStorage.getItem('user.access'))
            localStorage.setItem('user.refresh', sessionStorage.getItem('user.refresh'))
            localStorage.setItem('user.id', sessionStorage.getItem('user.id'))
            localStorage.setItem('user.name', sessionStorage.getItem('user.name'))
            localStorage.setItem('user.email', sessionStorage.getItem('user.email'))
            localStorage.setItem('user.isAuthenticated', sessionStorage.getItem('user.isAuthenticated')) 
            localStorage.setItem('user.helmholtz', sessionStorage.getItem('user.helmholtz')) 
        },
        setTokenSession(data) {
            console.log('setTokenSession', data)

            this.user.access = data['user.access']
            this.user.refresh = data['user.refresh']
            this.user.isAuthenticated = data['user.isAuthenticated']
            this.user.id = data['user.id']
            this.user.name = data['user.name']
            this.user.email = data['user.email']
            this.user.helmholtz = data['user.helmholtz']

            sessionStorage.setItem('user.access', data['user.access'])
            sessionStorage.setItem('user.refresh', data['user.refresh'])
            sessionStorage.setItem('user.id', data['user.id'])
            sessionStorage.setItem('user.name', data['user.name'])
            sessionStorage.setItem('user.email', data['user.email'])
            sessionStorage.setItem('user.isAuthenticated', data['user.isAuthenticated'])
            sessionStorage.setItem('user.helmholtz', data['user.helmholtz'])
        },
        setTokenLocal(data) {
            console.log('setTokenSession', data)

            this.user.access = data['user.access']
            this.user.refresh = data['user.refresh']
            this.user.isAuthenticated = data['user.isAuthenticated']
            this.user.id = data['user.id']
            this.user.name = data['user.name']
            this.user.email = data['user.email']

            localStorage.setItem('user.access', data['user.access'])
            localStorage.setItem('user.refresh', data['user.refresh'])
            localStorage.setItem('user.id', data['user.id'])
            localStorage.setItem('user.name', data['user.name'])
            localStorage.setItem('user.email', data['user.email'])
            localStorage.setItem('user.isAuthenticated', data['user.isAuthenticated'])
        },          
        removeToken() {
            console.log('removeToken')
            this.user.access = null
            this.user.refresh = null
            this.user.isAuthenticated = false
            this.user.id = null
            this.user.name = null
            this.user.email = null
            this.user.avatar = null
            localStorage.removeItem('user.access')
            localStorage.removeItem('user.refresh')
            localStorage.removeItem('user.id')
            localStorage.removeItem('user.name')
            localStorage.removeItem('user.email')
            localStorage.removeItem('user.isAuthenticated')
            localStorage.setItem('user.access', '')
            localStorage.setItem('user.refresh', '')
            localStorage.setItem('user.id', '')
            localStorage.setItem('user.name', '')
            localStorage.setItem('user.email', '')
            localStorage.setItem('user.avatar', '')
            localStorage.setItem('user.helmholtz', 'false')
            axios.defaults.headers.common['Authorization'] = ''
            
        },
        removeStoreSession() {
            console.log('removeTokenSession')
            this.user.access = null
            this.user.refresh = null
            this.user.isAuthenticated = false
            this.user.id = null
            this.user.name = null
            this.user.email = null
            this.user.avatar = null
            sessionStorage.removeItem('user.access')
            sessionStorage.removeItem('user.refresh')
            sessionStorage.removeItem('user.id')
            sessionStorage.removeItem('user.name')
            sessionStorage.removeItem('user.email')
            sessionStorage.removeItem('user.isAuthenticated')
            
            sessionStorage.setItem('user.access', '')
            sessionStorage.setItem('user.refresh', '')
            sessionStorage.setItem('user.id', '')
            sessionStorage.setItem('user.name', '')
            sessionStorage.setItem('user.email', '')
            sessionStorage.setItem('user.avatar', '')
            sessionStorage.setItem('user.helmholtz', 'false')
            axios.defaults.headers.common['Authorization'] = ''
        },        
        setUserInfo(user) {
            console.log('setUserInfo', user)
            this.user.id = user.id
            this.user.name = user.name
            this.user.email = user.email
            this.user.avatar = user.avatar

            localStorage.setItem('user.id', user.id)
            localStorage.setItem('user.name', user.name)
            localStorage.setItem('user.email', user.email)
            localStorage.setItem('user.avatar', user.avatar)

            console.log('User', this.user)
            
        },
        refreshToken() {
            axios.post('/api/refresh/', {
                refresh: this.user.refresh
            })
            .then(response => {
                this.user.access = response.data.access
                localStorage.setItem('user.access', response.data.access)
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.access
            })
            .catch((error) => {
                console.log(error)
                console.log(typeof this.user.helmholtz)
                if (this.user.helmholtz !== 'true') {
                    this.removeToken()
                }
                if (this.user.helmholtz == 'true') {
                    //TODO: should be more functional based on exiration date #24
                    console.log('helmholtz')
                }                
                
            })
        },

    }
})