import { resolve } from 'path'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJSX from '@vitejs/plugin-vue-jsx'
import * as path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), vueJSX()],
  base: '/uiadmin/',
  build: {
    outDir: resolve(__dirname, 'dist/admin'),
  },
  resolve: {
    alias: {
      '@': resolve(__dirname, 'src'),
    },
  },
  server: {
    host: true,
    port: 5173,
    secure: false,
    strictPort: true,
//    hmr: {
//      port: 5173,
//      protocol: "ws",
//    },
    hmr: { overlay: false },
  },
  optimizeDeps: {
    exclude: ['.out'],
  },


})
